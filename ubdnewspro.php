<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              #
 * @since             1.0.0
 * @package           wpgc
 *
 * @wordpress-plugin
 * Plugin Name:       UBD News Pro
 * Plugin URI:        #
 * Description:       UBD News Pro For Custom Post
 */

// DIR
define( 'UBD_BASE_DIR',plugin_dir_path( __FILE__ ). '/' );
define( 'UBD_INC', UBD_BASE_DIR . 'includes/' );

// URL
define( 'UBD_BASE_URL', plugin_dir_url( __FILE__ ));

require UBD_BASE_DIR . 'includes/class-ubd-init.php';

function run_ubd_init() {
	$obj_plugin = new UbdInit();
}

run_ubd_init();