<?php

class UbdAdmin{

    public function __construct() {         
        $this->init();
    }

    public function init(){
        $this->load_dependencies();
        $this->load_meta_boxes();
        add_action( 'admin_menu', [$this,'register_admin_menu'] );
    }

	private function load_dependencies() {
		require_once UBD_BASE_DIR . 'includes/post_types/ad_type.php';		
		new AdType();   		
		require_once UBD_BASE_DIR . 'includes/post_types/image_placement.php';		
		new ImagePlacement();   		
		require_once UBD_BASE_DIR . 'includes/post_types/recording_type.php';		
		new RecordingType();   		
		require_once UBD_BASE_DIR . 'includes/post_types/recording.php';		
		new Recording();   		
		require_once UBD_BASE_DIR . 'includes/post_types/supporting_clip.php';		
		new SupportingClip();   		
		require_once UBD_BASE_DIR . 'includes/post_types/video_placement.php';		
		new VideoPlacement();   		
		require_once UBD_BASE_DIR . 'includes/post_types/video.php';		
		new Video();   		
		require_once UBD_BASE_DIR . 'includes/post_types/video_type.php';		
		new VideoType();   		
		require_once UBD_BASE_DIR . 'includes/post_types/sponsor.php';		
		new Sponsor();   		
		require_once UBD_BASE_DIR . 'includes/post_types/sponsor_ad.php';		
		new SponsorAd();   		
		require_once UBD_BASE_DIR . 'includes/post_types/scripts.php';		
		new Script();   		
    }

    private function load_meta_boxes(){        
        require_once UBD_BASE_DIR . 'includes/meta_boxes/meta_box.php'; 
        foreach($this->umb_post_types() as $post_type){
            $config = json_decode(file_get_contents(UBD_BASE_DIR . "includes/config_genrate/{$post_type}.json"),true);	    
            new Meta_Box($post_type,$config); 
        }
    }
    
    private function umb_post_types(){
        return array(
            'ad_type',
            'image_placement',
            'recording_type',
            'recording',
            'scripts',
            'sponsor_ad',
            'sponsor',
            'supporting_clip',
            'video_placement',
            'video_type',
            'video',
        );
    }
    
    public function register_admin_menu(){
        $parent_slug="edit.php?post_type=ad_type";
        add_menu_page(
            __( 'Scripts', 'UBW' ),
            'Ubd Scripts',
            'manage_options',
            'edit.php?post_type=scripts',
            '',
            'dashicons-align-left',
            16
        );
        add_menu_page(
            __( 'Videos', 'UBW' ),
            'Ubd Videos',
            'manage_options',
            'edit.php?post_type=video',
            '',
            'dashicons-align-left',
            17
        );
        add_menu_page(
            __( 'Recordings', 'UBW' ),
            'Ubd Recordings',
            'manage_options',
            'edit.php?post_type=recording',
            '',
            'dashicons-align-left',
            18
        );
        add_menu_page(
            __( 'Sponsors', 'UBW' ),
            'Ubd Sponsors',
            'manage_options',
            'edit.php?post_type=sponsor',
            '',
            'dashicons-align-left',
            19
        );
        add_menu_page(
            __( 'Sponsor Ads', 'UBW' ),
            'Ubd Sponsor Ads',
            'manage_options',
            'edit.php?post_type=sponsor_ad',
            '',
            'dashicons-align-left',
            19
        );
        add_menu_page(
            __( 'Ubd Pro', 'UBW' ),
            'Ubd Pro',
            'manage_options',
            $parent_slug,
            '',
            'dashicons-align-left',
            19  
        );
        add_submenu_page( 
            $parent_slug, 
            'Manage AD Types', 
            'Manage AD Types',
            'manage_options', 
            'edit.php?post_type=ad_type'
        );
        add_submenu_page( 
            $parent_slug, 
            'Manage Image Placements', 
            'Manage Image Placements',
            'manage_options', 
            'edit.php?post_type=image_placement'
        );
        add_submenu_page( 
            $parent_slug, 
            'Manage Recording Types', 
            'Manage Recording Types',
            'manage_options', 
            'edit.php?post_type=recording_type'
        );
        add_submenu_page( 
            $parent_slug, 
            'Manage Supporting Clips', 
            'Manage Supporting Clips',
            'manage_options', 
            'edit.php?post_type=supporting_clip'
        );
        add_submenu_page( 
            $parent_slug, 
            'Manage Video Placements', 
            'Manage Video Placements',
            'manage_options', 
            'edit.php?post_type=video_placement'
        );
        add_submenu_page( 
            $parent_slug, 
            'Manage Video Types', 
            'Manage Video Types',
            'manage_options', 
            'edit.php?post_type=video_type'
        );
        add_submenu_page( 
            $parent_slug, 
            'Manage Sponsor Ads', 
            'Manage Sponsor Ads',
            'manage_options', 
            'edit.php?post_type=sponsor_ad'
        );
    }
}