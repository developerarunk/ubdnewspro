<?php

class Recording{   
    public function __construct() {         
        $this->init();
    }

    public function init(){
        add_action( 'init', [$this,'register_post'], 0 );
    }

    public function register_post(){
        // Register Custom Post Type
            $labels = array(
                'name'                  => _x( 'Recordings', 'Post Type General Name', 'UBW' ),
                'singular_name'         => _x( 'Recording', 'Post Type Singular Name', 'UBW' ),
                'menu_name'             => __( 'Recordings', 'UBW' ),
                'name_admin_bar'        => __( 'Recording', 'UBW' ),
                'archives'              => __( 'Recording Archives', 'UBW' ),
                'attributes'            => __( 'Recording Attributes', 'UBW' ),
                'parent_item_colon'     => __( 'Parent Recording:', 'UBW' ),
                'all_items'             => __( 'All Recordings', 'UBW' ),
                'add_new_item'          => __( 'Add New Recording', 'UBW' ),
                'add_new'               => __( 'Add Recording', 'UBW' ),
                'new_item'              => __( 'New Recording', 'UBW' ),
                'edit_item'             => __( 'Edit Recording', 'UBW' ),
                'update_item'           => __( 'Update Recording', 'UBW' ),
                'view_item'             => __( 'View Recording', 'UBW' ),
                'view_items'            => __( 'View Recording', 'UBW' ),
                'search_items'          => __( 'Search Recording', 'UBW' ),
                'not_found'             => __( 'Not found', 'UBW' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'UBW' ),
                'featured_image'        => __( 'Featured Image', 'UBW' ),
                'set_featured_image'    => __( 'Set featured image', 'UBW' ),
                'remove_featured_image' => __( 'Remove featured image', 'UBW' ),
                'use_featured_image'    => __( 'Use as featured image', 'UBW' ),
                'insert_into_item'      => __( 'Insert into Recording', 'UBW' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Recording', 'UBW' ),
                'items_list'            => __( 'Recording list', 'UBW' ),
                'items_list_navigation' => __( 'Recordings list navigation', 'UBW' ),
                'filter_items_list'     => __( 'Filter Recordings list', 'UBW' ),
            );
            $args = array(
                'label'                 => __( 'Recording', 'UBW' ),
                'description'           => __( 'Recording Description', 'UBW' ),
                'labels'                => $labels,
                'supports'              => array( 'title'),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => false,
                'menu_position'         => 5,
                'show_in_admin_bar'     => false,
                'show_in_nav_menus'     => false,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'recording', $args );
    }
}