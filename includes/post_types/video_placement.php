<?php

class VideoPlacement{
    public function __construct() {         
        $this->init();
    }

    public function init(){
        add_action( 'init', [$this,'register_post'], 0 );
    }

    public function register_post(){
        // Register Custom Post Type
            $labels = array(
                'name'                  => _x( 'Video Placements', 'Post Type General Name', 'UBW' ),
                'singular_name'         => _x( 'Video Placement', 'Post Type Singular Name', 'UBW' ),
                'menu_name'             => __( 'Video Placements', 'UBW' ),
                'name_admin_bar'        => __( 'Video Placement', 'UBW' ),
                'archives'              => __( 'Video Placement Archives', 'UBW' ),
                'attributes'            => __( 'Video Placement Attributes', 'UBW' ),
                'parent_item_colon'     => __( 'Parent Video Placement:', 'UBW' ),
                'all_items'             => __( 'All Video Placements', 'UBW' ),
                'add_new_item'          => __( 'Add New Video Placement', 'UBW' ),
                'add_new'               => __( 'Add Video Placement', 'UBW' ),
                'new_item'              => __( 'New Video Placement', 'UBW' ),
                'edit_item'             => __( 'Edit Video Placement', 'UBW' ),
                'update_item'           => __( 'Update Video Placement', 'UBW' ),
                'view_item'             => __( 'View Video Placement', 'UBW' ),
                'view_items'            => __( 'View Video Placement', 'UBW' ),
                'search_items'          => __( 'Search Video Placement', 'UBW' ),
                'not_found'             => __( 'Not found', 'UBW' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'UBW' ),
                'featured_image'        => __( 'Featured Image', 'UBW' ),
                'set_featured_image'    => __( 'Set featured image', 'UBW' ),
                'remove_featured_image' => __( 'Remove featured image', 'UBW' ),
                'use_featured_image'    => __( 'Use as featured image', 'UBW' ),
                'insert_into_item'      => __( 'Insert into Video Placement', 'UBW' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Video Placement', 'UBW' ),
                'items_list'            => __( 'Video Placement list', 'UBW' ),
                'items_list_navigation' => __( 'Video Placements list navigation', 'UBW' ),
                'filter_items_list'     => __( 'Filter Video Placements list', 'UBW' ),
            );
            $args = array(
                'label'                 => __( 'Video Placement', 'UBW' ),
                'description'           => __( 'Video Placement Description', 'UBW' ),
                'labels'                => $labels,
                'supports'              => array( 'title'),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => false,
                'menu_position'         => 5,
                'show_in_admin_bar'     => false,
                'show_in_nav_menus'     => false,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'video_placement', $args );
    }
}