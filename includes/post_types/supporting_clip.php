<?php

class SupportingClip{
    public function __construct() {         
        $this->init();
    }

    public function init(){
        add_action( 'init', [$this,'register_post'], 0 );
    }

    public function register_post(){
        // Register Custom Post Type
            $labels = array(
                'name'                  => _x( 'Supporting Clips', 'Post Type General Name', 'UBW' ),
                'singular_name'         => _x( 'Supporting Clip', 'Post Type Singular Name', 'UBW' ),
                'menu_name'             => __( 'Supporting Clips', 'UBW' ),
                'name_admin_bar'        => __( 'Supporting Clip', 'UBW' ),
                'archives'              => __( 'Supporting Clip Archives', 'UBW' ),
                'attributes'            => __( 'Supporting Clip Attributes', 'UBW' ),
                'parent_item_colon'     => __( 'Parent Supporting Clip:', 'UBW' ),
                'all_items'             => __( 'All Supporting Clips', 'UBW' ),
                'add_new_item'          => __( 'Add New Supporting Clip', 'UBW' ),
                'add_new'               => __( 'Add Supporting Clip', 'UBW' ),
                'new_item'              => __( 'New Supporting Clip', 'UBW' ),
                'edit_item'             => __( 'Edit Supporting Clip', 'UBW' ),
                'update_item'           => __( 'Update Supporting Clip', 'UBW' ),
                'view_item'             => __( 'View Supporting Clip', 'UBW' ),
                'view_items'            => __( 'View Supporting Clip', 'UBW' ),
                'search_items'          => __( 'Search Supporting Clip', 'UBW' ),
                'not_found'             => __( 'Not found', 'UBW' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'UBW' ),
                'featured_image'        => __( 'Featured Image', 'UBW' ),
                'set_featured_image'    => __( 'Set featured image', 'UBW' ),
                'remove_featured_image' => __( 'Remove featured image', 'UBW' ),
                'use_featured_image'    => __( 'Use as featured image', 'UBW' ),
                'insert_into_item'      => __( 'Insert into Supporting Clip', 'UBW' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Supporting Clip', 'UBW' ),
                'items_list'            => __( 'Supporting Clip list', 'UBW' ),
                'items_list_navigation' => __( 'Supporting Clips list navigation', 'UBW' ),
                'filter_items_list'     => __( 'Filter Supporting Clips list', 'UBW' ),
            );
            $args = array(
                'label'                 => __( 'Supporting Clip', 'UBW' ),
                'description'           => __( 'Supporting Clip Description', 'UBW' ),
                'labels'                => $labels,
                'supports'              => array( 'title'),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => false,
                'menu_position'         => 5,
                'show_in_admin_bar'     => false,
                'show_in_nav_menus'     => false,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'supporting_clip', $args );
    }
}