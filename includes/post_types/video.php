<?php

class Video{
    public function __construct() {         
        $this->init();
    }

    public function init(){
        add_action( 'init', [$this,'register_post'], 0 );
    }

    public function register_post(){
        // Register Custom Post Type
            $labels = array(
                'name'                  => _x( 'Videos', 'Post Type General Name', 'UBW' ),
                'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'UBW' ),
                'menu_name'             => __( 'Videos', 'UBW' ),
                'name_admin_bar'        => __( 'Video', 'UBW' ),
                'archives'              => __( 'Video Archives', 'UBW' ),
                'attributes'            => __( 'Video Attributes', 'UBW' ),
                'parent_item_colon'     => __( 'Parent Video:', 'UBW' ),
                'all_items'             => __( 'All Videos', 'UBW' ),
                'add_new_item'          => __( 'Add New Video', 'UBW' ),
                'add_new'               => __( 'Add Video', 'UBW' ),
                'new_item'              => __( 'New Video', 'UBW' ),
                'edit_item'             => __( 'Edit Video', 'UBW' ),
                'update_item'           => __( 'Update Video', 'UBW' ),
                'view_item'             => __( 'View Video', 'UBW' ),
                'view_items'            => __( 'View Video', 'UBW' ),
                'search_items'          => __( 'Search Video', 'UBW' ),
                'not_found'             => __( 'Not found', 'UBW' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'UBW' ),
                'featured_image'        => __( 'Featured Image', 'UBW' ),
                'set_featured_image'    => __( 'Set featured image', 'UBW' ),
                'remove_featured_image' => __( 'Remove featured image', 'UBW' ),
                'use_featured_image'    => __( 'Use as featured image', 'UBW' ),
                'insert_into_item'      => __( 'Insert into Video', 'UBW' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Video', 'UBW' ),
                'items_list'            => __( 'Video list', 'UBW' ),
                'items_list_navigation' => __( 'Videos list navigation', 'UBW' ),
                'filter_items_list'     => __( 'Filter Videos list', 'UBW' ),
            );
            $args = array(
                'label'                 => __( 'Video', 'UBW' ),
                'description'           => __( 'Video Description', 'UBW' ),
                'labels'                => $labels,
                'supports'              => array( 'title'),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => false,
                'menu_position'         => 5,
                'show_in_admin_bar'     => false,
                'show_in_nav_menus'     => false,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'video', $args );
    }
}