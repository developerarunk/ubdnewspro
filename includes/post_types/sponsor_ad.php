<?php

class SponsorAd{
    public function __construct() {         
        $this->init();
    }

    public function init(){
        add_action( 'init', [$this,'register_post'], 0 );
    }

    public function register_post(){
        // Register Custom Post Type
            $labels = array(
                'name'                  => _x( 'Sponsor Ads', 'Post Type General Name', 'UBW' ),
                'singular_name'         => _x( 'Sponsor Ad', 'Post Type Singular Name', 'UBW' ),
                'menu_name'             => __( 'Sponsor Ads', 'UBW' ),
                'name_admin_bar'        => __( 'Sponsor Ad', 'UBW' ),
                'archives'              => __( 'Sponsor Ad Archives', 'UBW' ),
                'attributes'            => __( 'Sponsor Ad Attributes', 'UBW' ),
                'parent_item_colon'     => __( 'Parent Sponsor Ad:', 'UBW' ),
                'all_items'             => __( 'All Sponsor Ads', 'UBW' ),
                'add_new_item'          => __( 'Add New Sponsor Ad', 'UBW' ),
                'add_new'               => __( 'Add Sponsor Ad', 'UBW' ),
                'new_item'              => __( 'New Sponsor Ad', 'UBW' ),
                'edit_item'             => __( 'Edit Sponsor Ad', 'UBW' ),
                'update_item'           => __( 'Update Sponsor Ad', 'UBW' ),
                'view_item'             => __( 'View Sponsor Ad', 'UBW' ),
                'view_items'            => __( 'View Sponsor Ad', 'UBW' ),
                'search_items'          => __( 'Search Sponsor Ad', 'UBW' ),
                'not_found'             => __( 'Not found', 'UBW' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'UBW' ),
                'featured_image'        => __( 'Featured Image', 'UBW' ),
                'set_featured_image'    => __( 'Set featured image', 'UBW' ),
                'remove_featured_image' => __( 'Remove featured image', 'UBW' ),
                'use_featured_image'    => __( 'Use as featured image', 'UBW' ),
                'insert_into_item'      => __( 'Insert into Sponsor Ad', 'UBW' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Sponsor Ad', 'UBW' ),
                'items_list'            => __( 'Sponsor Ad list', 'UBW' ),
                'items_list_navigation' => __( 'Sponsor Ads list navigation', 'UBW' ),
                'filter_items_list'     => __( 'Filter Sponsor Ads list', 'UBW' ),
            );
            $args = array(
                'label'                 => __( 'Sponsor Ad', 'UBW' ),
                'description'           => __( 'Sponsor Ad Description', 'UBW' ),
                'labels'                => $labels,
                'supports'              => array( 'title'),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => false,
                'menu_position'         => 5,
                'show_in_admin_bar'     => false,
                'show_in_nav_menus'     => false,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'sponsor_ad', $args );
    }
}