<?php

class Sponsor{
    public function __construct() {         
        $this->init();
    }

    public function init(){
        add_action( 'init', [$this,'register_post'], 0 );
    }

    public function register_post(){
        // Register Custom Post Type
            $labels = array(
                'name'                  => _x( 'Sponsors', 'Post Type General Name', 'UBW' ),
                'singular_name'         => _x( 'Sponsor', 'Post Type Singular Name', 'UBW' ),
                'menu_name'             => __( 'Sponsors', 'UBW' ),
                'name_admin_bar'        => __( 'Sponsor', 'UBW' ),
                'archives'              => __( 'Sponsor Archives', 'UBW' ),
                'attributes'            => __( 'Sponsor Attributes', 'UBW' ),
                'parent_item_colon'     => __( 'Parent Sponsor:', 'UBW' ),
                'all_items'             => __( 'All Sponsors', 'UBW' ),
                'add_new_item'          => __( 'Add New Sponsor', 'UBW' ),
                'add_new'               => __( 'Add Sponsor', 'UBW' ),
                'new_item'              => __( 'New Sponsor', 'UBW' ),
                'edit_item'             => __( 'Edit Sponsor', 'UBW' ),
                'update_item'           => __( 'Update Sponsor', 'UBW' ),
                'view_item'             => __( 'View Sponsor', 'UBW' ),
                'view_items'            => __( 'View Sponsor', 'UBW' ),
                'search_items'          => __( 'Search Sponsor', 'UBW' ),
                'not_found'             => __( 'Not found', 'UBW' ),
                'not_found_in_trash'    => __( 'Not found in Trash', 'UBW' ),
                'featured_image'        => __( 'Featured Image', 'UBW' ),
                'set_featured_image'    => __( 'Set featured image', 'UBW' ),
                'remove_featured_image' => __( 'Remove featured image', 'UBW' ),
                'use_featured_image'    => __( 'Use as featured image', 'UBW' ),
                'insert_into_item'      => __( 'Insert into Sponsor', 'UBW' ),
                'uploaded_to_this_item' => __( 'Uploaded to this Sponsor', 'UBW' ),
                'items_list'            => __( 'Sponsor list', 'UBW' ),
                'items_list_navigation' => __( 'Sponsors list navigation', 'UBW' ),
                'filter_items_list'     => __( 'Filter Sponsors list', 'UBW' ),
            );
            $args = array(
                'label'                 => __( 'Sponsor', 'UBW' ),
                'description'           => __( 'Sponsor Description', 'UBW' ),
                'labels'                => $labels,
                'supports'              => array( 'title'),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => false,
                'menu_position'         => 5,
                'show_in_admin_bar'     => false,
                'show_in_nav_menus'     => false,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'sponsor', $args );
    }
}