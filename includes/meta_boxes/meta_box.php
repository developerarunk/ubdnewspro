<?php

class Meta_Box{ 

    public $post_type;
    public $config;
    public $field_column;
    /**
     * Constructor.
     */
    public function __construct($post_type,$config) {
        $this->post_type=$post_type;
        $this->config=$config;
        if ( is_admin() ) {
            add_action( 'load-post.php',     array( $this, 'init_metabox' ) );
            add_action( 'load-post-new.php', array( $this, 'init_metabox' ) );
            add_action('admin_enqueue_scripts', array( $this, 'meta_admin_scripts' ));            
            // Add the custom columns to the post type:
            add_filter( "manage_{$this->post_type}_posts_columns", array( $this,'set_custom_columns' ));
            // Add the data to the custom columns for the post type:
            add_action( "manage_{$this->post_type}_posts_custom_column" , array( $this,'custom_column'), 10, 2 );
            // Add the sort:
            add_filter( "manage_edit-{$this->post_type}_sortable_columns", array( $this,'sortable_columns'));

            add_action("wp_ajax_{$this->post_type}", array($this,"ajax_post"));
        }
 
    }

    public function meta_admin_scripts(){
        if ( ! did_action( 'wp_enqueue_media' ) ) {
            wp_enqueue_media();
        }
        wp_enqueue_style("sortable-jquery-ui","//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
        wp_enqueue_style("ubd-styles",UBD_BASE_URL.'assets/admin/css/style.css');
        wp_enqueue_script('my-upload', UBD_BASE_URL.'assets/admin/js/upload_media_select.js', array('jquery'));
        wp_enqueue_script('my-sortable', UBD_BASE_URL.'assets/admin/js/sortable.js', array('jquery','jquery-ui-sortable'));
        wp_enqueue_style( 'select2css', UBD_BASE_URL.'assets/admin/css/select2.min.css', false, '1.0', 'all' );
        wp_enqueue_script( 'select2', UBD_BASE_URL.'assets/admin/js/selectWoo.full.js', array( 'jquery' ), '1.0', true );        
        wp_enqueue_style('thickbox');
        wp_enqueue_script('thickbox');
    }

    public function ajax_post(){
        $this->save_ajax_action();
        ?>
        <form action="" method="post">
        <table class="form-table" role="presentation">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="title"> Name  </label>
                    </th>
                    <td>
                        <input type="text" id="title" name="post_title" value="" size="30" />
                    </td>
                </tr>
            </tbody>
        </table>
        <?php
        $this->render_metabox(array());
        ?>
        <input type="hidden" name="post_type" value="<?php echo $_REQUEST['action']; ?>" />
        <input type="hidden" name="post_field" value="<?php echo $_REQUEST['field']; ?>" />
        <input type="hidden" name="submit" value="1" />
        <button name='submit' class="create_ajax_post ubd-btn-submit">Save</button>
        </form>
        <script>
        jQuery( document ).ready(function( $ ) {
            $( ".create_ajax_post" ).click(function(e){
                e.preventDefault();
                var data=$(this).parent().serialize();
                //console.log('data',data);
                $.ajax("<?php echo admin_url('admin-ajax.php?action='.$_REQUEST['action']); ?>", {
                    type: 'POST',  // http method
                    data: data,  // data to submit
                    success: function (data, status, xhr) {
                        if(data.ID){
                            var button = $("#<?php echo $_REQUEST['field'];?>").siblings( ".ubd-multi-sortable" );  
                            var removeBtn=$("<span class='ui-icon ubd-multi-close ui-icon-close'></span>"); 
                            var field="<?php echo $_REQUEST['field'];?>[]"; 
                            removeBtn.click(ubdMultiCloseHandler);             
                            var inputField=$("<input type='hidden' name="+field+" value="+data.ID+">");
                            var el=$("<li class='ui-state-default' data-post_id="+data.ID+"><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>"+data.post_title+"</li>").append(removeBtn).append(inputField);     
                            button.append(el);
                            jQuery("#TB_closeWindowButton").click();  
                        }                    
                    }
                });
                function ubdMultiCloseHandler(){
                    var item=$(this).parent(".ui-state-default");
                    var select2=item.parent(".ubd-multi-sortable").siblings( ".ubd-select2" );
                    var id=item.data("post_id");
                    var text=item.text();
                    var el=$("<option></option>");
                    el.text(text);
                    el.val(id);
                    select2.append(el);
                    //select2.find("option[value="+id+"]").prop('selected',false);
                    select2.trigger('change');  
                    item.remove();      
                }
            });
        });
    </script>
        <?php
        
        exit;
    }

    public function save_ajax_action(){
        if(isset($_POST['submit']) && $_POST['post_type']){
            $post_data = array(
                'post_title' => $_POST['post_title'],
                'post_type' => $_POST['post_type'],
                'post_status'   => 'publish'
            );
            $post_id = wp_insert_post( $post_data );
            if($post_id){
                $this->save_opertion($post_id);
                $post=get_post($post_id);
                wp_send_json($post);
            }            
        }
    }

    public function set_custom_columns($columns) {
        foreach($this->fields() as $field){
            if(isset($field['column']) && $field['column']){
                $this->field_column=$field['column'];
                $columns[$field['post_name']]=$field['post_title'];
            }      
        }
        unset($columns['date']);
        return $columns;
    }

    public function sortable_columns($columns) {
        foreach($this->fields() as $field){
            if(isset($field['column']) && $field['column']){
                $this->field_column=$field['column'];
                $columns[$field['post_name']]=$field['post_name'];
            }      
        }
        return $columns;
    }

    public function custom_column($column, $post_id ){
        foreach($this->fields() as $field){
            if($field['post_name']==$column){
                if($field['pick_object']=='pod' && $field['pick_val']!='' ){
                    $p_id=get_post_meta( $post_id , $column , true );
                    if($p_id){
                        echo get_post($p_id)->post_title;      
                    }
                }else if($field['pick_object']=='user'){
                    $user_id=get_post_meta( $post_id , $column , true );
                    if($user_id){
                        echo get_user_by('id',$user_id)->display_name;
                    }
                }else if($field['type']=='file'){
                    echo get_post_meta( $post_id , $column , true );
                }else if($field['type']=='boolean'){
                    $status=get_post_meta( $post_id , $column , true );
                    if($status){echo "Yes";}
                }else if($field['type']=='paragraph'){
                    echo get_post_meta( $post_id , $column , true );
                }else if($field['type']=='datetime'){
                    echo get_post_meta( $post_id , $column , true );
                }else{
                    echo get_post_meta( $post_id , $column , true );
                }                   
            }
        }
    }
 

    /**
     * Meta box initialization.
     */
    public function init_metabox() {        
        add_action( 'add_meta_boxes', array( $this, 'add_metabox'  )        );
        add_action( 'save_post',      array( $this, 'save_metabox' ), 10, 2 );
    }

    public function generate_post_select($select_id, $post_type, $selected = array(),$field) {
        if(isset($field['pick_format_type']) && $field['pick_format_type']=="multi"){
            $post_type_object = get_post_type_object($post_type);
            $label = $post_type_object->label;
            $posts = get_posts(array('post_type'=> $post_type, 'post_status'=> 'publish', 'suppress_filters' => false, 'posts_per_page'=>-1));
            echo '<select class="select2 ubd-select2" multiple name="'. $select_id .'" id="'.$select_id.'">';
            echo '<option value = "" >All '.$label.' </option>';
            foreach ($posts as $post) {
                if(($selected == $post->ID || in_array($post->ID,$selected) )) continue;
                echo '<option value="', $post->ID, '"', ($selected == $post->ID || in_array($post->ID,$selected) ) ? ' selected="selected"' : '', '>', $post->post_title, '</option>';
            }
            echo '</select>';
            ?>
            <ul class="ubd-multi-sortable">
                <?php foreach($selected as $post_id){ ?>
                    <li class="ui-state-default" data-post_id="<?php echo $post_id; ?>">
                        <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        <?php echo get_post($post_id)->post_title; ?>
                        <span class='ui-icon ubd-multi-close ui-icon-close'></span>
                        <input type='hidden' name="<?php echo $select_id; ?>[]" value="<?php echo $post_id; ?>">
                    </li>
                <?php } ?>
                
            </ul>  
            <?php if(isset($field['pick_allow_add_new']) && $field['pick_allow_add_new']){ ?>          
            <a href="<?php echo admin_url('admin-ajax.php?action='.$post_type.'&field='.$select_id); ?>" class="thickbox ubd-btn-submit" title="Add <?php echo $label; ?>"  class="thickbox"> 
                Add New
            </a>  
            <?php } ?>      
        <?php
        }else{                    
            $post_type_object = get_post_type_object($post_type);
            $label = $post_type_object->label;
            $posts = get_posts(array('post_type'=> $post_type, 'post_status'=> 'publish', 'suppress_filters' => false, 'posts_per_page'=>-1));
            echo '<select class="select2 ubd-select2" name="'. $select_id .'" id="'.$select_id.'">';
            echo '<option value = "" >All '.$label.' </option>';
            foreach ($posts as $post) {
                echo '<option value="', $post->ID, '"', ($selected == $post->ID || in_array($post->ID,$selected) ) ? ' selected="selected"' : '', '>', $post->post_title, '</option>';
            }
            echo '</select>';        
        }
    }

    public function generate_user_select($select_id, $selected = 0) {
        $users = get_users();
        echo '<select class="select2 ubd-select2" name="'. $select_id .'" id="'.$select_id.'">';
        echo '<option value = "" >All Users </option>';
        foreach ($users as $user) {
            echo '<option value="', $user->ID, '"', $selected == $user->ID ? ' selected="selected"' : '', '>', $user->display_name, '</option>';
        }
        echo '</select>';
    }
 
    /**
     * Adds the meta box.
     */
    public function add_metabox() {
        add_meta_box(
            $this->post_type.'-meta-box',
            __( 'Fields', 'textdomain' ),
            array( $this, 'render_metabox' ),
            $this->post_type,
            'advanced',
            'default'
        );
    }
 
    public function fields(){           
        return $this->config;     
    }
    /**
     * Renders the meta box.
     */
    public function render_metabox( $post ) {
        // Add an nonce field so we can check for it later.
        wp_nonce_field( "{$this->post_type}_inner_custom_box", "{$this->post_type}_inner_custom_box_nonce" );
        // Display the form, using the current value.
        ?>
        <table class="form-table" role="presentation">
            <tbody>
        <?php

        foreach($this->fields() as $field):
        // Use get_post_meta to retrieve an existing value from the database.
        $value = get_post_meta( $post->ID, $field['post_name'], true );
        ?>
        <tr>
            <th scope="row">
                <label for="<?php echo $field['post_name']; ?>">
                    <?php echo $field['post_title']; ?>
                    <?php if($field['required']) echo '<abbr title="required" class="required">*</abbr>';  ?>
                </label>
            </th>
            <td>
                <?php if($field['pick_object']=='pod' && $field['pick_val']!='' ):?>
                <?php $this->generate_post_select($field['post_name'],$field['pick_val'],$value,$field); ?>
                <?php elseif($field['pick_object']=='user'): ?>
                <?php $this->generate_user_select($field['post_name'],$value); ?>
                <?php elseif($field['type']=='file' && isset($field['multiple']) && $field['multiple']=='true'): ?>
                    <ul class="sortable ubd-sortable" data-field="<?php echo $field['post_name']; ?>">
                    <?php 
                        $image_id=$value;
                        if(!empty($value)){
                            ?>

                                <?php
                                foreach($value as $image_id){
                                    if( $image = wp_get_attachment_image_src( $image_id ) ) {
                                        ?>
                                        <li class="ui-state-default ui-sortable-handle">
                                            <span class="ui-icon ui-icon-arrow-4"></span>
                                            <span class="ui-icon ui-icon-close media-remove"></span>
                                            <input type="hidden" name="<?php echo $field['post_name']; ?>[]" value="<?php echo $image_id; ?>" />
                                            <img src="<?php echo $image[0]; ?>" style="display:block;max-width: 150px;height: 150px;object-fit: cover;margin-right: 5px;" />
                                        </li>
                                        <?php
                                        }      
                                }
                                ?>
                            <?php                        
                        }
                    ?> 
                    </ul>
                    <button class="misha-multi-upl ubd-btn-submit">Add Images</button>
                <?php elseif($field['type']=='file'): ?>
                <?php 
                    $image_id=$value;
                    if( $image = wp_get_attachment_image_src( $image_id ) ) {
                    echo '<a href="javascript:void(0)" class="misha-upl ubd-btn-submit"><img src="' . $image[0] . '" style="max-width:250px;" /></a>
                        <a href="javascript:void(0)" class="misha-rmv">Remove Attachemnt</a>
                        <input type="hidden" name="'.$field['post_name'].'" value="' . $image_id . '">';
                    } else {
                    echo '<a href="javascript:void(0)" class="misha-upl ubd-btn-submit">Upload Attachemnt</a>
                        <a href="javascript:void(0)" class="misha-rmv" style="display:none">Remove Attachemnt</a>
                        <input type="hidden" name="'.$field['post_name'].'" value="">';
                    } 
                ?>                        
                <?php elseif($field['type']=='boolean'): ?>
                    <input type="checkbox" id="<?php echo $field['post_name']; ?>" name="<?php echo $field['post_name']; ?>" value="true" <?php echo $value==true?"checked":"";  ?>/>   Yes 
                <?php elseif($field['type']=='paragraph'): ?>
                    <textarea id="<?php echo $field['post_name']; ?>" cols="60" rows="10" name="<?php echo $field['post_name']; ?>" ><?php echo esc_attr( $value ); ?></textarea>    
                <?php elseif($field['type']=='datetime'): ?>
                    <input type="datetime-local" id="<?php echo $field['post_name']; ?>" name="<?php echo $field['post_name']; ?>" value="<?php echo esc_attr( $value ); ?>" <?php if($field['required']) echo 'required'; ?> /> 
                <?php else: ?>
                    <input type="text" id="<?php echo $field['post_name']; ?>" name="<?php echo $field['post_name']; ?>" value="<?php echo esc_attr( $value ); ?>" <?php if($field['required']) echo 'required'; ?> size="50" />    
                <?php endif; ?>
            </td>
        </tr>                
        <?php endforeach; ?>
        </tbody>
        </table>
        <?php
    }
 
    /**
     * Handles saving the meta box.
     *
     * @param int     $post_id Post ID.
     * @param WP_Post $post    Post object.
     * @return null
     */
    public function save_metabox( $post_id, $post ) {
        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */
        // Check if our nonce is set.
        if ( ! isset( $_POST["{$this->post_type}_inner_custom_box_nonce"] ) ) {
            return $post_id;
        }
 
        $nonce = $_POST["{$this->post_type}_inner_custom_box_nonce"];
 
        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, "{$this->post_type}_inner_custom_box" ) ) {
            return $post_id;
        }
 
        /*
         * If this is an autosave, our form has not been submitted,
         * so we don't want to do anything.
         */
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }
 
        // Check the user's permissions.
        if ( 'page' == $_POST['post_type'] ) {
            if ( ! current_user_can( 'edit_page', $post_id ) ) {
                return $post_id;
            }
        } else {
            if ( ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }
 
        /* OK, it's safe for us to save the data now. */    
        $this->save_opertion($post_id);

    }

    public function save_opertion($post_id){
        // Update the meta field.
        foreach($this->fields() as $field){
            $data = $_POST[$field['post_name']];
            if($field['type']=='boolean' && !isset($_POST[$field['post_name']])){                
                $data = false;
            }       
            // Use get_post_meta to retrieve an existing value from the database.

            update_post_meta($post_id, $field['post_name'], $data);
        }
    }
}