
jQuery( document ).ready(function( $ ) {
    $( ".ubd-sortable" ).sortable();
    $( ".ubd-multi-sortable" ).sortable();  
    $(".media-remove").click(remove_parent);
    function remove_parent(){        
        $(this).parent().remove();        
    }
    
    //var eventSelect=$( "#supporting_clip");
    var eventSelect=$( ".ubd-select2");
    // eventSelect.selectWoo({
    //     placeholder:"Search Supporting Clip...",
    // });
    eventSelect.selectWoo({
        placeholder:"Search ..."
    });
    eventSelect.on("select2:select", function (e) { add_ui("select2:select", e); });
    $("ul.select2-choices").sortable({
        containment: 'parent'
    });    
    function add_ui(name, evt) {
        var multiple=$(evt.currentTarget).attr('multiple');
        if(!multiple){
            return;      
        }
        var field=$(evt.currentTarget).attr('id')+"[]";
        if (!evt) {
            var args = "{}";
            } else {
            var args = JSON.stringify(evt.params, function (key, value) {
                if (value && value.nodeName) return "[DOM node]";
                if (value instanceof $.Event) return "[$.Event]";
                return value;
            });
        }
        var button = $(evt.currentTarget).siblings( ".ubd-multi-sortable" );  
        var removeBtn=$("<span class='ui-icon ubd-multi-close ui-icon-close'></span>");  
        removeBtn.click(ubdMultiCloseHandler);             
        var inputField=$("<input type='hidden' name="+field+" value="+evt.params.data.id+">");
        var el=$("<li class='ui-state-default' data-post_id="+evt.params.data.id+"><span class='ui-icon ui-icon-arrowthick-2-n-s'></span>"+evt.params.data.text+"</li>").append(removeBtn).append(inputField);     
        button.append(el);  
        var element = evt.params.data.element;
        var $element = $(element);
        $element.detach();
        $(this).trigger("change");      
    }
    $(".ubd-multi-close").on("click",ubdMultiCloseHandler);
    function ubdMultiCloseHandler(){
        var item=$(this).parent(".ui-state-default");
        var select2=item.parent(".ubd-multi-sortable").siblings( ".ubd-select2" );
        var id=item.data("post_id");
        var text=item.text();
        var el=$("<option></option>");
        el.text(text);
        el.val(id);
        select2.append(el);
        //select2.find("option[value="+id+"]").prop('selected',false);
        select2.trigger('change');  
        item.remove();      
    }

} );