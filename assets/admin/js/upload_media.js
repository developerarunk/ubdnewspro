jQuery(document).ready( function( $ ) {

    $('.upload_image_button').click(function() {
        var upload_btn=$(this).data('upload');
        console.log("upload_btn",upload_btn);
        formfield = $('#'+upload_btn).attr('name');
        tb_show( '', 'media-upload.php?type=image&amp;TB_iframe=true' );
        window.send_to_editor = function(html) {
           imgurl = $(html).attr('src');
           console.log("imgurl",html);                
           $('#'+upload_btn).val(imgurl);
           tb_remove();
        }
        return false;
    });

});