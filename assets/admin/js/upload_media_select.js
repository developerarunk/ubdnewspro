jQuery(function($){
 
	// on upload button click
	$('body').on( 'click', '.misha-upl', function(e){
 
		e.preventDefault();
 
		var button = $(this),
		custom_uploader = wp.media({
			title: 'Insert image',
			library : {
				// uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
				type : 'image'
			},
			button: {
				text: 'Use this image' // button label text
			},
			multiple: false
		}).on('select', function() { // it also has "open" and "close" events
			var attachment = custom_uploader.state().get('selection').first().toJSON();
			button.html('<img src="' + attachment.url + '" style="max-width:250px;" >').next().show().next().val(attachment.id);
		}).open();
 
	});

	// on upload button click
	$('body').on( 'click', '.misha-multi-upl', function(e){
		e.preventDefault();	
		var button = $(this).siblings( ".sortable" );
		var field=button.data('field')+"[]";
		custom_uploader = wp.media({
			title: 'Insert image',
			library : {
				// uploadedTo : wp.media.view.settings.post.id, // attach to the current post?
				type : 'image'
			},
			button: {
				text: 'Use this image' // button label text
			},
			multiple: true
		}).on('select', function() { // it also has "open" and "close" events
		var selection = custom_uploader.state().get('selection');
		selection.map( function( attachment ) {
		  attachment = attachment.toJSON();	
		  var removeBtn=$("<span class='ui-icon ubd-multi-close ui-icon-close'></span>");  
		  removeBtn.click(function(){
			$(this).parent().remove(); 
		  });  
		  var el=$("<li class='ui-state-default'><span class='ui-icon ui-icon-arrow-4'></span></li>");
		  el.append(removeBtn);	
		  el.append("<input type='hidden' name="+field+" value="+attachment.id+"><img src=" +attachment.url+" style='display:block;max-width: 150px;height: 150px;object-fit: cover;margin-right: 5px;'>")
		  button.append(el);
		});		
        button.sortable('refresh');
		}).open();
	
	});
 
	// on remove button click
	$('body').on('click', '.misha-rmv', function(e){
 
		e.preventDefault();
 
		var button = $(this);
		button.next().val(''); // emptying the hidden field
		button.hide().prev().html('Upload image');
	});
 
});